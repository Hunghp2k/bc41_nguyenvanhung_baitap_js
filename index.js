/**Bài 1: Tiền lương nhân viên
 * - Đầu vào: 
 * Lương 1 ngày: 100.000
 * Số ngày làm: 30 ngày
 * b1:tạo biến soluong và gán biến soluong = 100000
 * b2:tạo biến songay và gán biến songay=30
 * b3:tạo biến tienluong
 * b4:tính giá tri tienluong
 * b5:in ra kết quả tienluong 
 * - Đầu ra:
 * Tiên lương của nhân viên = 3000000
 */
var soluong = document.getElementById('soluong');//tạo biến tiền lương 1 ngày
var songay = document.getElementById('songay');//tạo biến số ngày làm việc
var tienluong = document.getElementById('tienluong'); //khai báo biến tiền lương
function doi() {
    console.log("tiền lương", tienluong.value = songay.value * soluong.value);
}
/**Bài 2: Tính giá trị trung bình
 * Nhập 5 số thực
  * so1 = 10
  * so2 = 20
  * so3 = 30
  * so4 = 40
  * so5 = 50
  * b1:tạo biến so1,so2,so3,so4,so5 và gán giá trị cho so1,so2,so3,so4,so5
  * b2:tạo biến soTB
  * b3:tính giá trị soTB
  * b4:in ra kết quả
  * - Đầu ra 
  * Số Trung bình = 30
 */

var so1 = document.getElementById("so1");
var so2 = document.getElementById("so2");
var so3 = document.getElementById("so3");
var so4 = document.getElementById("so4");
var so5 = document.getElementById("so5");
var TB = document.getElementById("TB");
function tinh() {
    console.log('trung bình', TB.value =
        (so1.value * 1 + so2.value * 1 + so3.value * 1 + so4.value * 1 + so5.value * 1) / 5);
}

/**Bài 3: Quy đổi tiền
  * - Đầu ra
  * Gán thẻ usa
  * Gán thẻ vnd
  * b1:tạo biến usd
  * b2:tạo biến vnd
  * b3:tính giá trị vnd đổi được và in ra kết quả
  * - Đầu ra
  * vnd = usd * 23500.
  */
var usd = document.getElementById("usd");
var vnd = document.getElementById("vnd");
function quyDoi() {
    console.log('tiền đổi được', vnd.value = usd.value * 23500)
}
/**Bài 4: Tính chu vi, diện tích HCN
 * - Đầu vào:
 * Chiều dài : 20
 * Chiều rộng: 10
 * b1:tạo biến giá trị chieudai, chieurong và gán giá trị cho chieudai, chieurong
 * b2: tạo biến chuvi, dientich
 * b3:tính giá trị chuvi, dientich
 * b4:in ra kết quả
 * - Đầu ra:
 * Diện tích: 200
 * Chu vi: 60
 */
var cd = document.getElementById("cd");
var cr = document.getElementById("cr");
var cv = document.getElementById("cv");
var dt = document.getElementById("dt");
function ketqua() {
    console.log("chu vi", cv.value = (cd.value * 1 + cr.value * 1) * 2);
    console.log("dientich", dt.value = cd.value * cr.value);
}
/**Bài 5: Tính tổng 2 kí số vừa nhập
  * - Đầu vào
  * Cho giá trị = 28
  * b1:tạo biến number và gán giá trị
  * b2:tạo biến donVi và tính giá trị
  * b3:tạo biến hangchuc và tính giá trị
  * b4:tạo biến tongkyso và tính giá trị
  * b5:in ra kết quả
  * - Đầu ra 
  * Kết quả = 10
  */


function tong() {
    var so2chuso = document.getElementById("so2chuso").value;
    var a = Math.floor(so2chuso / 10);
    var b = so2chuso % 10;
    var tong = a + b;
    document.getElementById("tong").innerHTML = tong.toLocaleString();
}


